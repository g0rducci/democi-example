# CREACION DE VPC
resource "aws_vpc" "vpc_ejemplo" { # Crea VPC 
  cidr_block = "10.0.0.0/16"  # Define el CIDR

  tags = {
    Name = "exercise_1_example" # Tagea el resource
  }
}

# KeyPair para bastiones
resource "aws_key_pair" "bastion_key" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChq9mfp3YpB61C84zDJbRhWBoPWIo+QLzXiieNSih91EmHQQZUF8rlXojXyyVRsNKQL98JoTtX5tk2qZjRQ0ahopY8rr9Owb6x2UblUy2EROgtMTEyVPH0Ex3HJoX6MtEBTrhFmL8WqFdPSyD90vojVJ2DmVlNA9jf9d+PJCiTOISmD9m80lfC6ByYkElLFmzJky9LBWpiz6u0oQY7v+PM6QEu8dWYO9l19jXqY3vIyjK2QQIxCmSGkLf80SW1tz7RjSna5waFOspXPbqRTiMwwEz5mRwngOfFrOs4b0ujIQxF97FobYihraEnp6vYDYA/7K4kdIkm8k4DStzGDMOAND/qqsvZT4hwzkD4IfEmOePLdpb4PwJZiZJZgvfHzhI28ChR/MSKyiRsPtXrAQr5dyE8vR9KfDJt2jPDO0f7rJo9DIoJgLbfT4wicPcG5qBoqfSX26teCSu2DkAOus/gGLYaCvdH99CB4J75wit/xb/+IEP+gSJ+kCkm3dCvgEk= yair rodriguez@DESKTOP-N1MJN6F"
}

# Creacion public subnet 1
resource "aws_subnet" "public_subnet_1" {
  vpc_id            = aws_vpc.vpc_ejemplo.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
}

# Creacion de INTERNET subred publica
resource "aws_internet_gateway" "example_internet_gateway" {
  vpc_id = aws_vpc.vpc_ejemplo.id

  tags = {
    Name = "internet-gateway-subnet-public-vpc"
  }
}

# Creacion de Tabla de enrutamiento Publica a internet subnets publicas

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc_ejemplo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_internet_gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.example_internet_gateway.id
  }

  tags = {
    Name = "Public Route Table"
  }
}

# Asociacion de la RT a la Public Subnet
resource "aws_route_table_association" "public_1_rt_a" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_rt.id
}

# Creación del grupo de seguridad para bastión SG
resource "aws_security_group" "bastion_sg" {
  vpc_id = aws_vpc.vpc_ejemplo.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2422
    to_port     = 2422
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Creación de instancias EC2 para Bastion en subnets públicas

resource "aws_eip" "bastion_eip_1" {
  vpc      = true
  instance = aws_instance.bastion_instance_1.id
}

resource "aws_instance" "bastion_instance_1" {
  ami           = "ami-053b0d53c279acc90"  
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_subnet_1.id
  key_name      = aws_key_pair.bastion_key.key_name


  # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  tags = {
    "Name" = "bastion-instance-1"
  }
}